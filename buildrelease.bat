cordova build --release android

cd platforms/android/ant-build

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore "D:\LarsKristian\Documents\Dropbox\AndroidKeystore\mobilars-release-key.keystore" PersonalHub-release-unsigned.apk mobilars

cp PersonalHub-release-unsigned.apk PersonalHub.apk
scp PersonalHub.apk larsr@tgs-fhir-web.testsenter.nhn.no:/usr/share/nginx/html/apk

cd ../../..
